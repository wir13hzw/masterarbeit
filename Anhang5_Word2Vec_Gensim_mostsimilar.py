import gensim
from gensim.models import Word2Vec
import glob
import os
import nltk.data

society = ''

searchterm = ''

print(society)
print(searchterm + '\n')

def remove_items(test_list, item):
    res = [i for i in test_list if i != item]
    return res


path = './Result_OCR/' + society

for filename in glob.glob(os.path.join(path, '*.txt')):
    list1 = []
    list2 = []
    with open(filename, 'r', encoding='utf8') as magazine_text:
        magazine_text = magazine_text.read()

        list1 = nltk.tokenize.sent_tokenize(magazine_text)

        for sentence in list1:
            list1_split = sentence.split(' ')
            for word in list1_split:
                if word == '.' or word == '':
                    list1_split = remove_items(list1_split, word)

# input format: list of list of tokens
            list2.append(list1_split)

# establish model
    model = gensim.models.Word2Vec(window=6, min_count=1, hs=0)
    model.build_vocab(list2, progress_per=1000)
    model.train(list2, total_examples=model.corpus_count, epochs=model.epochs)

# If the word does not appear in the text, an error occurs. Step to avoid:
    isInText = False
    for sentence in list2:
        for word in sentence:
            if word == searchterm:
                isInText = True
                break
        if isInText:
            break

    if isInText:
        print(filename.replace('./Result_OCR/' + society + '\\' + society,'').replace('.txt',''))
# 10 most similar words
        print(model.wv.most_similar(searchterm))
    else:
        print(filename.replace('./Result_OCR/' + society + '\\' + society,'').replace('.txt',''))
        print('Word does not appear in text.')


# optional: further searchterms