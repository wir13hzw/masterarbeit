from nltk.stem import WordNetLemmatizer
from HanTa import HanoverTagger as ht
import spacy
import glob
import os

society = ''
language = ''
if society == 'Berlin':
    language = 'german'
elif society == 'London':
    language = 'english'
elif society == 'Paris':
    language = 'french'

path_stopwords = './stopwords/languages/' + language + '.txt'
if os.path.exists(path_stopwords):
    with open(path_stopwords, 'r',  encoding='utf8') as stopwords:
        stopwords = stopwords.read()


def lemmatizing(language, magazine_word):
    if language == 'german':
        lemmatizer = ht.HanoverTagger('morphmodel_ger.pgz')

        magazine_word = str(lemmatizer.analyze(magazine_word))
        magazine_word = magazine_word.replace("('", '').split("'", 1)
        magazine_word = magazine_word[0]
        return magazine_word

    elif language == 'english':
        lemmatizer = WordNetLemmatizer()

        magazine_word = lemmatizer.lemmatize(magazine_word)
        return magazine_word

    elif language == 'french':
        lemmatizer = spacy.load('fr_core_news_sm')
        magazine_words = lemmatizer(magazine_word)

        for magazine_word in magazine_words:
            magazine_word = str(magazine_word.lemma_)
        return magazine_word


path = './Result_OCR/' + society

for filename in glob.glob(os.path.join(path, '*.txt')):
    comparisonset = set()
    with open(filename, 'r', encoding='utf8') as magazine_text:
        magazine_text = magazine_text.read()
        magazine_text = magazine_text.replace('-\n', '').replace('- \n', '').replace(',', '').replace(';', '').replace(':', '').replace('(', '').replace(')', '').replace("'", " ").replace('"', '')
        magazine_text = magazine_text.replace('\n', ' ')
        magazine_text = ' '.join(magazine_text.split())
        magazine_text = ''.join([i for i in magazine_text if not i.isdigit()])

# remove stopwords
        magazine_text_split = magazine_text.split(' ')
        for magazine_word in magazine_text_split:
            for stopword in stopwords.split('\n'):
                if magazine_word == stopword:
                    magazine_text = magazine_text.replace(' ' + magazine_word + ' ', ' ')

# lemmatizing
        magazine_text_split = magazine_text.split(' ')
        for magazine_word in magazine_text_split:
            if magazine_word not in comparisonset:
                magazine_word_lemma = lemmatizing(language, magazine_word)
                magazine_text = magazine_text.replace(' ' + magazine_word + ' ', ' ' + magazine_word_lemma + ' ')
                comparisonset.add(magazine_word)
        magazine_text = magazine_text.lower()

        filename_new = filename.replace('.txt', '')
        filename_new = filename_new + '_edited.txt'
        if not os.path.exists(filename_new):
            with open(filename_new, 'w', encoding='utf8') as magazine_text_new:
                magazine_text_new.write(magazine_text)
                print('Done: ' + filename_new.replace('./Result_OCR/' + society + '\\', ''))